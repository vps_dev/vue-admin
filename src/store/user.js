import USERS_MOCK from "../mock/users";

export default {
  namespaced: true,
  state: {
    user: null
  },
  actions: {
    getUser({ commit }, id) {
      setTimeout(() => {
        const users = [...USERS_MOCK];
        const user = users.find(item => item.id === id);
        commit("setUser", { user });
      }, 500);
    },
    create({ dispatch, commit }, user) {
      dispatch("loading/show", null, { root: true });

      setTimeout(() => {
        commit("setUser", { user });

        dispatch("loading/hide", null, { root: true });
        dispatch("alert/success", "Creating successful", { root: true });
      }, 2000);
    },
    update({ dispatch, commit }, user) {
      dispatch("loading/show", null, { root: true });

      setTimeout(() => {
        commit("setUser", { user });

        dispatch("loading/hide", null, { root: true });
        dispatch("alert/success", "Updating successful", { root: true });
      }, 2000);
    }
  },
  mutations: {
    setUser(state, { user }) {
      state.user = user;
    }
  },
  getters: {}
};
