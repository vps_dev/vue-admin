import TRANSACTIONS_MOCK from "../mock/transactions";

export default {
  namespaced: true,
  state: {
    transactions: []
  },
  actions: {
    getTransactions({ commit }) {
      commit("removeAllTransactions");
      setTimeout(() => {
        const transactions = [...TRANSACTIONS_MOCK];
        commit("addTransactions", { transactions });
      }, 500);
    }
  },
  mutations: {
    addTransactions(state, { transactions }) {
      state.transactions.push(...transactions);
    },
    removeAllTransactions(state) {
      state.transactions.splice(0);
    }
  },
  getters: {}
};
