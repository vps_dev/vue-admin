import RESELLERS_MOCK from "../mock/resellers";

export default {
  namespaced: true,
  state: {
    resellers: []
  },
  actions: {
    getResellers({ commit }) {
      commit("removeAllResellers");
      setTimeout(() => {
        const resellers = [...RESELLERS_MOCK];
        commit("addResellers", { resellers });
      }, 500);
    }
  },
  mutations: {
    addResellers(state, { resellers }) {
      state.resellers.push(...resellers);
    },
    removeAllResellers(state) {
      state.resellers.splice(0);
    }
  },
  getters: {}
};
