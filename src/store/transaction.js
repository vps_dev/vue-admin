import TRANSACTION_DETAIL_MOCK from "../mock/transaction-detail";

export default {
  namespaced: true,
  state: {
    transaction: null
  },
  actions: {
    getTransaction({ commit }) {
      setTimeout(() => {
        const transaction = TRANSACTION_DETAIL_MOCK;
        commit("setTransaction", { transaction });
      }, 500);
    }
  },
  mutations: {
    setTransaction(state, { transaction }) {
      state.transaction = transaction;
    }
  },
  getters: {}
};
