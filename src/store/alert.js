import ALERT_TYPE from "../constants/alert-type";

export default {
  namespaced: true,
  state: {
    type: null,
    message: null
  },
  actions: {
    success({ commit }, message) {
      commit("success", message);
    },
    error({ commit }, message) {
      commit("error", message);
    },
    warning({ commit }, message) {
      commit("error", message);
    },
    info({ commit }, message) {
      commit("error", message);
    },
    clear({ commit }) {
      commit("clear");
    }
  },
  mutations: {
    success(state, message) {
      state.type = ALERT_TYPE.success;
      state.message = message;
    },
    error(state, message) {
      state.type = ALERT_TYPE.error;
      state.message = message;
    },
    warning(state, message) {
      state.type = ALERT_TYPE.warning;
      state.message = message;
    },
    info(state, message) {
      state.type = ALERT_TYPE.info;
      state.message = message;
    },
    clear(state) {
      state.type = null;
      state.message = null;
    }
  }
};
