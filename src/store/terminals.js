import TERMINALS_MOCK from "../mock/terminals";

export default {
  namespaced: true,
  state: {
    terminals: []
  },
  actions: {
    getTerminals({ commit }) {
      commit("removeAllTerminals");
      setTimeout(() => {
        const terminals = [...TERMINALS_MOCK];
        commit("addTerminals", { terminals: terminals });
      }, 500);
    },
    getTerminalsByFilter({ commit, dispatch }, { serial, name }) {
      if (!!serial && !!name) {
        dispatch("getTerminals");
        return;
      }

      setTimeout(() => {
        commit("removeAllTerminals");
        let terminals = [...TERMINALS_MOCK];
        if (serial) {
          terminals = terminals.filter(item => item.serial.includes(serial));
        }

        if (name) {
          terminals = terminals.filter(item =>
            item.reseller_name.includes(name)
          );
        }

        commit("addTerminals", { terminals: terminals });
      }, 500);
    },
    unpairTerminal({ commit }, terminalId) {
      setTimeout(() => {
        commit("unpairTerminalById", terminalId);
      }, 500);
    },
    registerTerminal({ dispatch }) {
      dispatch("loading/show", null, { root: true });

      setTimeout(() => {
        dispatch("loading/hide", null, { root: true });
        dispatch("alert/success", "Registered successful", { root: true });
      }, 2000);
    },
    createTerminal({ dispatch }) {
      dispatch("loading/show", null, { root: true });

      setTimeout(() => {
        dispatch("loading/hide", null, { root: true });
        dispatch("alert/success", "Created successful", { root: true });
      }, 2000);
    },
    boldSetupTerminal({ dispatch }) {
      dispatch("loading/show", null, { root: true });

      setTimeout(() => {
        dispatch("loading/hide", null, { root: true });
        dispatch("alert/success", "Setup successful", { root: true });
      }, 2000);
    }
  },
  mutations: {
    addTerminals(state, { terminals }) {
      state.terminals.push(...terminals);
    },
    removeAllTerminals(state) {
      state.terminals.splice(0);
    },
    unpairTerminalById(state, terminalId) {
      const index = state.terminals.findIndex(item => item.id === terminalId);
      if (index >= 0) {
        state.terminals[index].unpair = true;
      }
    }
  },
  getters: {}
};
