export default {
  namespaced: true,
  state: {
    show: false
  },
  actions: {
    show({ commit }) {
      commit("updateShow", true);
    },
    hide({ commit }) {
      commit("updateShow", false);
    }
  },
  mutations: {
    updateShow(state, show) {
      state.show = show;
    }
  }
};
