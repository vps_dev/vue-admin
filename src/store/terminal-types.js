import TERMINAL_TYPES_MOCK from "../mock/terminal-types";

export default {
  namespaced: true,
  state: {
    terminalTypes: []
  },
  actions: {
    getTerminalTypes({ commit }) {
      commit("removeAllTerminalTypes");
      setTimeout(() => {
        const terminalTypes = [...TERMINAL_TYPES_MOCK];
        commit("addTerminalTypes", { terminalTypes: terminalTypes });
      }, 500);
    }
  },
  mutations: {
    addTerminalTypes(state, { terminalTypes }) {
      state.terminalTypes.push(...terminalTypes);
    },
    removeAllTerminalTypes(state) {
      state.terminalTypes.splice(0);
    }
  }
};
