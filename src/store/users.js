import USERS_MOCK from "../mock/users";

export default {
  namespaced: true,
  state: {
    users: []
  },
  actions: {
    getUsers({ commit }) {
      commit("removeAllUsers");
      setTimeout(() => {
        const users = [...USERS_MOCK];
        commit("addUsers", { users });
      }, 500);
    },
    getUsersByFilter({ commit, dispatch }, { name, email }) {
      if (!!email && !!name) {
        dispatch("getUsers");
        return;
      }

      setTimeout(() => {
        commit("removeAllUsers");
        let users = [...USERS_MOCK];
        if (email) {
          users = users.filter(item => item.email.includes(email));
        }

        if (name) {
          users = users.filter(item => item.name.includes(name));
        }

        commit("addUsers", { users: users });
      }, 500);
    }
  },
  mutations: {
    addUsers(state, { users }) {
      state.users.push(...users);
    },
    removeAllUsers(state) {
      state.users.splice(0);
    }
  },
  getters: {}
};
