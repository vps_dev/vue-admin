import ROLES_MOCK from "../mock/roles";

export default {
  namespaced: true,
  state: {
    roles: []
  },
  actions: {
    getRoles({ commit }) {
      commit("removeAllRoles");
      setTimeout(() => {
        const roles = [...ROLES_MOCK];
        commit("addRoles", { roles });
      }, 500);
    }
  },
  mutations: {
    addRoles(state, { roles }) {
      state.roles.push(...roles);
    },
    removeAllRoles(state) {
      state.roles.splice(0);
    }
  },
  getters: {}
};
