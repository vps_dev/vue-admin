import router from "../router";
import { AUTH_TOKEN } from "../constants/config";

export default {
  namespaced: true,
  state: {
    user: null,
    token: localStorage.getItem(AUTH_TOKEN) || ""
  },
  actions: {
    login({ dispatch, commit }, { login, password }) {
      dispatch("loading/show", null, { root: true });

      setTimeout(() => {
        if (login === "123" && password === "123") {
          localStorage.setItem(AUTH_TOKEN, "token");

          commit("loginSuccess", "token");
          router.replace("/");
          dispatch("alert/success", "You have successfully logged in.", {
            root: true
          });
        } else {
          const error = "Incorrect Login or Password";
          commit("loginFailure");
          dispatch("alert/error", error, { root: true });
        }

        dispatch("loading/hide", null, { root: true });
      }, 1000);
    },
    logout({ commit }) {
      router.push("/login");
      localStorage.removeItem(AUTH_TOKEN);
      commit("logout");
    }
  },
  mutations: {
    loginSuccess(state, token) {
      state.token = token;
    },
    loginFailure(state) {
      state.token = "";
      state.user = null;
    },
    logout(state) {
      state.token = "";
      state.user = null;
    }
  },
  getters: {
    isLoggedIn: state => !!state.token
  }
};
