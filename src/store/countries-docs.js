import COUNTRIES_DOCS_MOCK from "../mock/countries-docs";

export default {
  namespaced: true,
  state: {
    countriesDocs: []
  },
  actions: {
    getCountriesDocs({ commit }) {
      commit("removeAllCountriesDocs");
      setTimeout(() => {
        const countriesDocs = [...COUNTRIES_DOCS_MOCK];
        commit("addCountriesDocs", { countriesDocs: countriesDocs });
      }, 500);
    }
  },
  mutations: {
    addCountriesDocs(state, { countriesDocs }) {
      state.countriesDocs.push(...countriesDocs);
    },
    removeAllCountriesDocs(state) {
      state.countriesDocs.splice(0);
    }
  }
};
