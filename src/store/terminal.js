import TERMINALS_MOCK from "../mock/terminals";

export default {
  namespaced: true,
  state: {
    terminal: null
  },
  actions: {
    getTerminal({ commit }, id) {
      setTimeout(() => {
        const terminals = [...TERMINALS_MOCK];
        const terminal = terminals.find(item => item.id === id);
        commit("setTerminal", { terminal });
      }, 500);
    },
    update({ dispatch, commit }, terminal) {
      dispatch("loading/show", null, { root: true });

      setTimeout(() => {
        commit("setTerminal", { terminal });
        dispatch("loading/hide", null, { root: true });
        dispatch("alert/success", "Updating successful", { root: true });
      }, 2000);
    }
  },
  mutations: {
    setTerminal(state, { terminal }) {
      state.terminal = terminal;
    }
  },
  getters: {}
};
