import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import { AUTH_TOKEN } from "./constants/config";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "login",
      component: () =>
        import(/* webpackChunkName: "login" */ "./views/Login.vue")
    },
    {
      path: "/users",
      name: "users",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "users" */ "./views/Users.vue")
    },
    {
      path: "/user/new",
      name: "userNew",
      component: () =>
        import(/* webpackChunkName: "userNew" */ "./views/UserNew.vue")
    },
    {
      path: "/user/:id",
      name: "user",
      component: () => import(/* webpackChunkName: "user" */ "./views/User.vue")
    },
    {
      path: "/terminals",
      name: "terminals",
      component: () =>
        import(/* webpackChunkName: "terminals" */ "./views/Terminals.vue")
    },
    {
      path: "/register-terminal",
      name: "registerTerminal",
      component: () =>
        import(/* webpackChunkName: "registerTerminal" */ "./views/RegisterTerminal.vue")
    },
    {
      path: "/terminal-settings/:id",
      name: "terminalSettings",
      component: () =>
        import(/* webpackChunkName: "terminalSettings" */ "./views/TerminalSettings.vue")
    },
    {
      path: "/transactions/",
      name: "transactions",
      component: () =>
        import(/* webpackChunkName: "transactions" */ "./views/Transactions.vue")
    },
    {
      path: "/transaction/:id",
      name: "transaction",
      component: () =>
        import(/* webpackChunkName: "transaction" */ "./views/Transaction.vue")
    },
    {
      path: "/terminal-virtual/:id",
      name: "terminalVirtual",
      component: () =>
        import(/* webpackChunkName: "terminalVirtual" */ "./views/TerminalVirtual.vue")
    },
    {
      path: "/terminal-create/",
      name: "terminalCreate",
      component: () =>
        import(/* webpackChunkName: "terminalCreate" */ "./views/TerminalCreate.vue")
    },
    {
      path: "/terminal-bolt-setup/",
      name: "terminalBoltSetup",
      component: () =>
        import(/* webpackChunkName: "terminalBoltSetup" */ "./views/TerminalBoltSetup.vue")
    }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ["/login"];
  const authRequired = !publicPages.includes(to.path);
  const isLoggedIn = localStorage.getItem(AUTH_TOKEN);

  if (authRequired && !isLoggedIn) {
    return next("/login");
  }

  next();
});

export default router;
