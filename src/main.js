import Vue from "vue";
import axios from "axios";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { AUTH_TOKEN } from "./constants/config";

Vue.config.productionTip = false;

const token = localStorage.getItem(AUTH_TOKEN);
if (token) {
  axios.defaults.headers.common["Authorization"] = token;
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
