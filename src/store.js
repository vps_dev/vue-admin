import Vue from "vue";
import Vuex from "vuex";
import account from "./store/account";
import alert from "./store/alert";
import countriesDocs from "./store/countries-docs";
import loading from "./store/loading";
import resellers from "./store/resellers";
import roles from "./store/roles";
import terminalTypes from "./store/terminal-types";
import terminal from "./store/terminal";
import terminals from "./store/terminals";
import transaction from "./store/transaction";
import transactions from "./store/transactions";
import user from "./store/user";
import users from "./store/users";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    account,
    alert,
    countriesDocs,
    loading,
    resellers,
    roles,
    terminalTypes,
    terminal,
    terminals,
    transaction,
    transactions,
    user,
    users
  }
});
