export const PASSWORD_RULES = [v => !!v || "Password is required"];

export const CONFIRM_PASSWORD_RULES = [
  v => !!v || "Confirm password is required"
];

export const LOGIN_RULES = [
  v => !!v || "Login is required",
  v => (v && v.length <= 20) || "Login must be less than 20 characters"
];

export const NAME_RULES = [
  v => !!v || "Name is required",
  v => (v && v.length <= 20) || "Name must be less than 20 characters"
];

export const EMAIL_RULES = [
  v => !!v || "E-mail is required",
  v =>
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      v
    ) || "E-mail must be valid"
];

export const ROLES_RULES = [v => !!v || "Role is required"];

export const RESELLER_RULES = [v => !!v || "Reseller is required"];

export const TERMINAL_SERIAL_RULES = [v => !!v || "Serial Number is required"];
export const ACCESS_TOKEN_RULES = [v => !!v || "Access Token is required"];
export const MERCHANT_ID_RULES = [v => !!v || "Merchant ID is required"];

export const AMOUNT_RULES = [v => !!v || "Amount is required"];

export const SERIAL_NUM_RULES = [v => !!v || "Serial number is required"];
export const TERMINAL_TYPE_RULES = [v => !!v || "Terminal type is required"];
