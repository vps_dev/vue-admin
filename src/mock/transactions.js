export default [
  {
    id: 1,
    status: "Approved",
    action: "Sale",
    amount: 1,
    auth_code: "F54355",
    duration: "00:00:30",
    date: "2/15/2019 3:45pm"
  },
  {
    id: 2,
    status: "Approved",
    action: "Sale",
    amount: 1,
    auth_code: "F54355",
    duration: "00:00:30",
    date: "2/15/2019 3:45pm"
  },
  {
    id: 3,
    status: "Declined",
    action: "Sale",
    amount: 1,
    auth_code: "",
    duration: "00:00:30",
    date: "2/15/2019 3:45pm"
  },
  {
    id: 4,
    status: "Cancelled",
    action: "Sale",
    amount: 1,
    auth_code: "",
    duration: "00:00:30",
    date: "2/15/2019 3:45pm"
  },
  {
    id: 5,
    status: "Error",
    action: "Sale",
    amount: 1,
    auth_code: "F54355",
    duration: "00:00:30",
    date: "2/15/2019 3:45pm"
  }
];
