export default [
  {
    id: 1,
    serial: "543234",
    reseller_name: "John's Pet Store",
    reseller_id: 6,
    is_virtual: true,
    unpair: false
  },
  {
    id: 2,
    serial: "872342",
    reseller_name: "Sam's Auto",
    reseller_id: 2,
    is_virtual: true,
    unpair: false
  },
  {
    id: 3,
    serial: "8234234234",
    reseller_name: "Steve's Deli",
    reseller_id: 4,
    is_virtual: false,
    unpair: false
  }
];
