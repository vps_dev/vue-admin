export default [
  {
    id: 123,
    name: "John Smith",
    email: "jsmith@somewhere.com",
    role: "Admin",
    active: true,
    last_login: "1/1/2019",
    reseller_id: 6
  },
  {
    id: 456,
    name: "Jane Smith",
    email: "janesmith@somewhere.com",
    role: "User",
    active: false,
    last_login: "2/1/2019",
    reseller_id: 2
  }
];
