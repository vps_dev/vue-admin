import Vue from "vue";
import Vuetify, { VList } from "vuetify/lib";
import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
  iconfont: "md",
  theme: {
    primary: "#35414d",
    secondary: "#93c01f"
  },
  components: {
    VList
  }
});
